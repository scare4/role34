# Role34

## Lancer le programme

Via Eclipse, tous les paquets doivent être contenus dans le paquet role34, et tous les dossiers doivent être contenus dans les paquets nommées role34.<nom de dossier>.
Lorsque l'architecture du projet est reconstruite, lancer le programme à partir du fichier Main.java.

## Règles du jeu

Vous êtes un duc avec de grandes ambitions, et après avoir vu l'inactivité de vos compères, vous décidez de prendre les devants!

Cependant, il semblerait que vous n'êtes pas le seul duc à s'être rendu compte de la situation...

Construisez des troupes pour vous défendre des attaques ennemies ou pour mener l'attaque, améliorez vos chateaux pour que l'argent coûle à flots, conquérez le monde et écrasez toutes ces contrées endormies!

## Informations de jeu

Vous posséderez les châteaux ayant un drapeau bleu tandis que l'ennemi possèdera les châteaux avec un drapeau rouge.
Les châteaux avec un drapeau blanc sont neutres, et n'attaqueront pas.
La partie peut être considérée finie lorsque le joueur ou l'ennemi ne possède plus de château.

La plupart des données de jeu dont vous avez besoin se trouveront en haut de l'écran.
ainsi, il vous sera possible d'afficher différentes données sur les armées et châteaux présents sur la carte de jeu, tel que sa composition de troupes, ou encore ses richesses et gains pour les châteaux.

Un appui sur la barre d'espace permettra de mettre le jeu en pause, ce qui vous permettra de planifier vos coups sereinement.

Chaque château a une réserve d'argent, et grâce à elle, vous êtes capable de recruter des soldats de différents types:
* Les piquiers, unités frêles mais peu chère à recruter! (100 florins)
* Les cavaliers, unités rapides et efficaces, mais demandant une meilleure part du gâteau! (500 florins)
* Les onagres, machines de guerres résistantes et dévastatrices mais ayant un coût de production faramineux! (1000 florins)

Chaque château peut aussi être amélioré, ce qui augmente les gains perçus au fur et à mesure du temps. Le coût d'amélioration d'un chateau est égal à 500 fois son niveau courant en florins.

Un double clic sur un château possédé par le joueur permet d'afficher une fenêtre qui permet de changer le type de production du château: n'importe quelle des trois unités citées ci-dessus peuvent être recrutées, le château peut lui-même être amélioré, ou aucune action peut être effectuée.
La production sera démarrée seulement si ce château a les fonds requis pour l'effectuer.
Le jeu sera mis en pause tant que cette fenêtre sera affichée.

Il est aussi possible d'attaquer un château non appartenu par le joueur.
Pour cela, sélectionnez le château allié qui enverra ses troupes, puis le château à attaquer.
Une fenêtre apparaîtra, ayant plusieurs sliders qui permettront de sélectionner le nombres d'unités à envoyer de chaque type d'unité.
Lorsque les unités ont été choisies, il suffit d'appuyer sur le bouton de confirmation pour que les unités se chargent en dehors de la forteresse, puis se dirige vers le château ennemi.
Si vos troupes sont assez fortes, il est possible que le château vous appartienne après quelques tours de combat acharné!

## Fonctionnalités mises en place

* Chaque château est possédé par un duc différent au début de la partie.
* Les unités peuvent être envoyés dans un château adverse, ce qui donnera lieu à un combat. Si les unités attaquantes éradiquent les unités du château, le château appartiendra au duc du château attaquant.
* L'appui sur la barre d'espace permet de mettre le jeu en pause.
* Il est possible de changer le mode de production d'un château possédé en double-cliquant dessus, ce qui permet de sélectionner un type d'unité, le château lui-même ou rien.
* L'amélioration d'un château est permanente, et génère plus de florins par heure.
* L'emplacement des châteaux ainsi que différentes données des châteaux neutres sont aléatoires, et changent à chaque partie.
* L'ennemi (château rouge) possède une intelligence artificielle difficile à vaincre par moments.

## Bogues

* Lors de l'affichage d'une fenêtre, si la fenêtre est fermée, il est possible qu'elle reste visible en tant que fenêtre fantôme. Nous pensons que le problème vient d'un problème de rafraîchissement de la fenêtre de jeu, car cette fenêtre fantôme disparaît si une unité est créée ainsi que dans d'autres cas plus aléatoires.
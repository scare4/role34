package role34;

import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.Slider;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import role34.map.Tile;
import role34.map.entities.Army;
import role34.map.entities.Castle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.binding.Bindings;

/** Army selection window. */
public class ArmySelector {
    /** Main box of this window. */
    private VBox selectorBox;
    /** Box with the confirm and cancel buttons. */
    private HBox buttonBox;

    /** Sliders used to know how many units of each type to send. */
    private Slider[] slider;

    /** Confirm button. */
    private Button okButton;
    /** Cancel button. */
    private Button noButton;

    /** Window's title. */
    private Text titleText = new Text();
    /** Texts displaying how mant units of each type are currently selected. */
    private Label resultText[] = new Label[UnitType.values().length];

    /** Castle sending the troops. */
    private Castle castle;
    /** Destination tile of the troops. */
    private Tile target;

    /**
     * Creates a prompt window that asks how many units should be sent.
     * @param castle Castle sending the troops.
     * @param target Destination tile of the troops.
     */
    public ArmySelector(Castle castle, Tile target) {
        Main.pause();
        this.castle = castle;
        this.target = target;

        selectorBox = new VBox();
        selectorBox.getStyleClass().add("window");
        selectorBox.setPrefSize(Settings.ARMY_SELECTOR_WIDTH, Settings.ARMY_SELECTOR_HEIGHT);

        titleText.setText("Select units to send.\n");

        slider = new Slider[UnitType.values().length];
        //UnitType.valueOf("PIKE").ordinal()
        for(int i = 0; i < UnitType.values().length; i++) {
            slider[i] = new Slider(0, castle.getUnitCount(UnitType.values()[i]), 0);
            slider[i].setShowTickLabels(true);
            slider[i].setBlockIncrement(1);

            resultText[i] = new Label();
            resultText[i].textProperty().bind(
                Bindings.format(
                    "%.0f %s\n",
                    slider[i].valueProperty(),
                    UnitType.values()[i]
                )
            );
        }

        okButton = new Button("Send");
        noButton = new Button("Cancel");

        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if ((int)slider[0].getValue() + (int)slider[1].getValue()  + (int)slider[2].getValue() < 1)
                    return;
                Army army = castle.sendArmy(target, (int)slider[0].getValue(), (int)slider[1].getValue(), (int)slider[2].getValue());
                if (army == null)
                    return;
                Main.root.getChildren().remove(selectorBox);
                Main.input.clearArmySelector();
                Main.unPause();
                Main.realm.getArmies().add(army);
            }
        });
        noButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Main.root.getChildren().remove(selectorBox);
                Main.input.clearArmySelector();
                Main.unPause();
            }
        });

        buttonBox = new HBox(okButton, noButton);

        selectorBox.getChildren().add(titleText);
        for(int i = 0; i < UnitType.values().length; i++) {
            selectorBox.getChildren().addAll(slider[i], resultText[i]);
        }
        selectorBox.getChildren().add(buttonBox);
        selectorBox.setLayoutX(Settings.SCENE_WIDTH / 2 - Settings.ARMY_SELECTOR_WIDTH / 2);
        selectorBox.setLayoutY(Settings.SCENE_HEIGHT / 2 - Settings.ARMY_SELECTOR_HEIGHT / 2);
        selectorBox.setAlignment(Pos.TOP_LEFT);
        Main.root.getChildren().add(selectorBox);
    }
}
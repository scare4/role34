package role34;

/** The different possible types of units. */
public enum UnitType {
    PIKE,
    HORSE,
    ONAGRE
}
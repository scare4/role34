package role34.map;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.layout.HBox;

import role34.Settings;
import role34.map.entities.Entity;

/** A tile. */
public class Tile {
    /** Horizontal position of this tile. */
    private double x;
    /** Vertical position of this tile. */
    private double y;
    /** Occupant of this tile. Can be null if the tile's free. */
    private Entity occupant = null;
    /** Graphical display box of the tile. */
    private HBox hbox;

    /** List of neighbour tiles. */
    private List<Tile> neighbours;

    /**
     * Returns the tile's horizontal position.
     * @return Tile's horizontal position.
     */
    public double getX() { return x; }
    /**
     * Returns the tile's vertical position.
     * @return Tile's vertical position.
     */
    public double getY() { return y; }
    /**
     * Returns the tile's occupant, or null if there isn't any.
     * @return Tile's occupant or null.
     */
    public Entity getOccupant() { return occupant; }
    /**
     * Returns the tile's graphical display box.
     * @return Tile's graphical display box.
     */
    public HBox getHbox() { return hbox; }

    /**
     * Sets the tile's new occupant.
     * @param occupant Tile's new occupant.
     */
    public void setOccupant(Entity occupant) { this.occupant = occupant; }
    /**
     * Sets the tile's new graphical display box.
     * @param hbox Tile's new graphical display box.
     */
    public void setHBox(HBox hbox) { this.hbox = hbox; }

    /**
     * Gets the on-screen Y position (including the offset caused by the interface).
     * @return On-screen Y position of this tile.
     */
    public double getGraphicalY() { return y + Settings.STATUS_BAR_HEIGHT; }

    /**
     * Moves the current occupant of the tile to another free tile.
     * @param destination Tile to move the occupant to.
     * @return True if moved, false otherwise.
     */
    public boolean moveOccupant(Tile destination) {
        if (destination.getOccupant() == null) {
            destination.setOccupant(occupant);
            occupant = null;
            return true;
        }
        return false;
    }

    /**
     * Returns the list of neighbours of this tile.
     * @return List of neighbours of this tile.
     */
    public List<Tile> getNeighbours() { return neighbours; }

    /**
     * Returns the direction a neighbour tile is from this tile (if the compared tile is not an actual aligned neighbour this function's behavior is undetermined)
     * @param neighbour Tile to compare with.
     * @return The neighbour's direction compared to this castle.
     */
    public Directions getNeighbourDirection(Tile neighbour) {
        if (neighbour.getY() > getY())      return Directions.SOUTH;
        else if (neighbour.getX() > getX()) return Directions.EAST;
        else if (neighbour.getY() < getY()) return Directions.NORTH;
        else                                return Directions.WEST;
    }

    /**
     * Checks if a tile is neighbouring this tile.
     * @param tile Tile to check for.
     * @return True if neighbour, false otherwise.
     */
    public boolean isNeighbour(Tile tile) { return neighbours.contains(tile); }

    /**
     * If a tile isn't a neighbour, adds it as such.
     * @param tile Tile to add as neighbour.
     */
    public void addNeighbour(Tile tile) {
        if (!isNeighbour(tile))
            neighbours.add(tile);
    }

    /**
     * Computes the distance between this tile and another.
     * @param tile Tile to measure the distance with.
     * @return Distance between the two tiles.
     */
    public double distance(Tile tile) {
        return Math.sqrt(
            Math.abs(Math.pow(x - tile.getX(), 2)) +
            Math.abs(Math.pow(y - tile.getY(), 2))
        );
    }

    /**
     * Checks if a set of coordinates is within the tile.
     * @param x Horizontal position.
     * @param y Vertical position.
     */
    public boolean proximity(double x, double y) {
        return x >= this.x && y >= this.y && x <= this.x + Settings.TILE_WIDTH && y <= this.y + Settings.TILE_WIDTH;
    }

    /**
     * Creates a new unoccupied tile.
     * @param x Horizontal position of the tile.
     * @param y Vertical position of the tile.
     */
    public Tile(double x, double y) {
        this.x = x;
        this.y = y;
        neighbours = new ArrayList<Tile>();
    }

    /**
     * Creates a new occupied tile.
     * @param x Horizontal position of the tile.
     * @param y Vertical position of the tile.
     * @param occupant Occupant of the tile.
     */
    public Tile(double x, double y, Entity occupant) {
        this.x = x;
        this.y = y;
        neighbours = new ArrayList<Tile>();
        this.occupant = occupant;
    }

    // The following is used for pathfinding

    /** Predecessor of this tile. Used in pathfinding. */
    private Tile predecessor;
    /** True if this tile is tagged, false otherwise. Used in pathfinding. */
    private boolean tag;

    /**
     * Returns this tile's predecessor. Used in pathfinding.
     * @return Tile's predecessor.
     */
    public Tile getPredecessor() { return predecessor; }
    /**
     * Returns true if the tile's tagged, false otherwise. Used in pathfinding.
     * @return True if the tile's tagged, false otherwise.
     */
    public boolean isTagged() { return tag; }

    /**
     * Sets the tile's predecessor. Used in pathfinding.
     * @param predecessor Tile's new predecessor.
     */
    public void setPredecessor(Tile predecessor) { this.predecessor = predecessor; }
    /** Tags this tile. Used in pathfinding. */
    public void tag() { tag = true; }
    /** Untags this tile. Used in pathfinding. */
    public void untag() { tag = false; }
}
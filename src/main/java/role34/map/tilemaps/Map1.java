package role34.map.tilemaps;

import role34.Settings;
import role34.map.Tile;

/** Example tilemap. */
public final class Map1 extends Tilemap {
    /**
     * Constructor for this map.
     * Contains all of this map's valuable data, including map size and tile data.
     */
    public Map1() {
        name = "Tilemap 1";
        xStart = 2;   yStart = 2;
        xTiles = 25;    yTiles = 18;
        xSpacing = 40; ySpacing = 40;
        duchies = xTiles * yTiles / 10;

        populateTiles();
        createDuchies();

        // Generate neighbours
        for (Tile tile : tileList) {
            for (Tile tile2 : tileList) {
                double d = tile.distance(tile2);
                if (d < Settings.MAX_CONTACT_DISTANCE && d > 0) {
                    tile.addNeighbour(tile2);
                    tile2.addNeighbour(tile);
                }
            }
        }
        populateCastles();
    }
}
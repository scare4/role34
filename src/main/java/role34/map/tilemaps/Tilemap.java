package role34.map.tilemaps;

import java.util.ArrayList;
import java.util.List;

import role34.duchy.*;
import role34.map.Tile;
import role34.map.entities.Castle;

/** Tilemap base. */
public abstract class Tilemap {
    /** List of tiles in this map. */
    protected List<Tile> tileList = new ArrayList<Tile>();
    /** List of castles in this map. */
    protected List<Castle> castleList = new ArrayList<Castle>();
    /** List of available duchies in this map. */
    protected List<Duchy> duchyList = new ArrayList<Duchy>();

    /** Map's name. */
    protected String name;
    /** Horizontal starting position of the first column of tiles. */
    protected int xStart;
    /** Vertical starting position of the first line of tiles. */
    protected int yStart;
    /** Amount of tiles per line. */
    protected int xTiles;
    /** Amount of tiles per column. */
    protected int yTiles;
    /** Horizontal spacing between two tiles. */
    protected int xSpacing;
    /** Vertical spacing between two tiles. */
    protected int ySpacing;
    /** Amount of duchies to create using createDuchies(). */
    protected int duchies;

    /**
     * Returns the list of tiles in this map.
     * @return List of tiles in this map.
     */
    public List<Tile> getTiles() { return tileList; }
    /**
     * Returns the list of castles in this map.
     * @return List of castles in this map.
     */
    public List<Castle> getCastles() { return castleList; }
    /**
     * Returns the list of available duchies in this map.
     * @return List of available duchies in this map.
     */
    public List<Duchy> getDuchies() { return duchyList; }
    /**
     * Returns the map's name.
     * @return Map's name.
     */
    public String getName() { return name; }

    /**
     * Adds a tile to the list of tiles in this map.
     * @param tile Tile to add.
     */
    public void addTile(Tile tile) { tileList.add(tile); }

    /** Creates all of the map's tiles and fills in the tile list. */
    public void populateTiles() {
        for (int j = 0; j < yTiles; j++)
            for (int i = 0; i < xTiles; i++)
                tileList.add(new Tile(xStart + i * xSpacing, yStart + j * ySpacing));
    }

    /** Creates all of the map's duchies and fills in the duchy list. */
    public void createDuchies() {
        for (int i = 0; i < duchies; i++) {
            if (i < duchies - 2)      duchyList.add(new Duchy("Duchy #" + (i + 1)));
            else if (i < duchies - 1) duchyList.add(new AI("Enemy"));
            else                      duchyList.add(new Player("Player"));
        }
    }

    /** Creates all of the map's castles, assigns a duchy to each of them and fills in the castle list. */
    public void populateCastles() {
        if (duchies > xTiles * yTiles)
            return;
        for (int i = 0; i < duchies; i++) {
            boolean checkN;
            int tileID;
            do {
                // Check if this tile and all surrounding tiles are free in order to select it
                checkN = false;
                tileID = role34.Main.random.nextInt(xTiles) + xTiles * role34.Main.random.nextInt(yTiles);
                int tempTileID = tileID - xTiles - 1;
                for (int j = 0; j < 9; j++) {
                    if (tempTileID >= 0 && tempTileID < xTiles * yTiles)
                        if (tileList.get(tempTileID).getOccupant() != null) {
                            checkN = true;
                            break;
                        }
                    if (j % 3 == 2) tempTileID += xTiles - 2;
                    else            tempTileID ++;
                }
            } while (checkN);

            Tile t = tileList.get(tileID);
            Castle c = new Castle(duchyList.get(i), t, duchyList.get(i) instanceof AI || duchyList.get(i) instanceof Player ? 1 : 2);
            castleList.add(c);
            t.setOccupant(c);
        }
    }
}
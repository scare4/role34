package role34.map;

/** Directions used by doors. */
public enum Directions {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
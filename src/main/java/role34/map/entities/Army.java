package role34.map.entities;

import java.util.LinkedList;

import javafx.scene.image.ImageView;

import role34.Main;
import role34.Settings;
import role34.Units;
import role34.duchy.Duchy;
import role34.UnitType;
import role34.map.Tile;

/** Army entity. */
public class Army extends Entity {
    /** Contains this army's units. */
    private Units[] units;
    /** If the army is mobile, stores the path it has to take to reach its destination. */
    private LinkedList<Tile> path = null;
    /** If the army is mobile, stores the army's destination. */
    private Tile destination = null;
    /** Formation progress while army exits castle. */
    private int formation;
    /** Duchy who owns the army. */
    private Duchy duchy;

    /**
     * Returns the army's units.
     * @return Army's troops.
     */
    public Units[] getUnits() { return units; }
    /**
     * Returns path.
     * @return path.
     */
    public LinkedList<Tile> getPath() { return path; }
    /**
     * Returns destination.
     * @return destination.
     */
    public Tile getDestination() { return destination; }
    /**
     * Returns formation progress.
     * @return formation progress.
     */
    public int getFormation() { return formation; }
    /**
     * Returns owner of the army.
     * @return owner of the army.
     */
    public Duchy getDuke() { return duchy; }

    /** Steps the formation of the army. */
    public void stepForm() {
        if (formation < Settings.DOOR_WIDTH) formation = 0;
        else                                 formation -= Settings.DOOR_WIDTH;
    }

    /** If the army is mobile, stores this army's sprite. */
    private ImageView armyView;

    /**
     * If the army is mobile, tries to find a path to a tile and moves to it.
     * @param tile Destination tile.
     * @return True if a path was found, false otherwise.
     */
    public boolean goTo(Tile tile) {
        LinkedList<Tile> queue = new LinkedList<Tile>();
        LinkedList<Tile> processed = new LinkedList<Tile>();

        this.tile.tag();
        queue.add(this.tile);

        // TODO: Take in account the other castle's entrance as well
        while (!queue.isEmpty()) {
            Tile current = queue.getFirst();
            for (Tile neighbour : current.getNeighbours()) {
                if (neighbour == tile) {
                    destination = tile;
                    path = new LinkedList<Tile>();
                    path.add(neighbour);
                    while (current != this.tile) {
                        path.addFirst(current);
                        current = current.getPredecessor();
                    }
                    for (Tile t : processed) t.untag();
                    for (Tile t : queue)     t.untag();
                    return true;
                }
                if (current.getOccupant() instanceof Castle && current.getNeighbourDirection(neighbour) != ((Castle)current.getOccupant()).getDoor())
                    continue;
                if (neighbour.getOccupant() != null)
                    continue;
                if (!neighbour.isTagged()) {
                    neighbour.tag();
                    neighbour.setPredecessor(current);
                    queue.add(neighbour);
                }
            }
            processed.add(queue.remove());
        }

        for (Tile t : processed) t.untag();
        for (Tile t : queue)     t.untag();

        return false;
    }

    /**
     * Calculates this army's cumulated power.
     * @return Cumulated power of this army.
     */
    public int calculatePower() {
        int p = 0;
        for(Units units : this.units) {
            p += units.getPower() * units.getAmount();
        }
        return p;
    }

    /**
     * If the army is mobile, moves the army to a given tile.
     * @param tile Destination tile.
     * @return Always returns true for now.
     */
    public boolean move(Tile tile) {
        armyView.relocate(tile.getX() + 2, tile.getGraphicalY() + 2);
        this.tile = tile;
        return true;
    }

    /**
     * Adds a given amount of troops of a certain type to the army and increases formation time accordingly.
     * @param type Type of the troop to add.
     * @param amount Amount of troops to add.
     */
    public void addTroop(UnitType type, int amount) {
        for (Units units : this.units)
            if (units.getType() == type) {
                units.setAmount(units.getAmount() + amount);
                formation += amount;
                return;
            }
    }

    /**
     * Merges this army with another (the army is not removed from the realm).
     * @param army Army to merge with.
     */
    public void mergeWith(Army army) {
        for (int i = 0; i < getUnits().length; i++)
            army.addTroop(UnitType.values()[i], units[i].getAmount());
        tile.setOccupant(null);
        Main.root.getChildren().remove(armyView);
    }

    /**
     * Constructor for a mobile army.
     * @param tile Starting tile of the army.
     */
    public Army(Tile tile) {
        this.tile = tile;
        units = Units.createEmptyUnits();
        armyView = new ImageView(Main.armyImage);
        duchy = ((Castle)tile.getOccupant()).getDuke();
        Main.root.getChildren().add(armyView);
    }

    /** Constructor for a static army. */
    public Army() {
        units = Units.createEmptyUnits();
    }
}
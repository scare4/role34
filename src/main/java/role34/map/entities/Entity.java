package role34.map.entities;

import role34.Sprite;
import role34.map.Tile;
import role34.Units;
import role34.UnitType;

/** Basic entity. */
public abstract class Entity {
    /** Entity's position. */
    Tile tile;
    /** Entity's sprite. */
    Sprite sprite;
    /** Entity can be traversed by another. */
    boolean passThrough;

    /**
     * Returns the entity's position.
     * @return Entity's position.
     */
    public Tile getTile() { return tile; }

    /**
     * Returns the entity's units.
     * @return Entity's troops.
     */
    public abstract Units[] getUnits();

    /**
     * Sets the entity's position.
     * @param tile Entity's new position.
     */
    public void setTile(Tile tile) { this.tile = tile; }
    /**
     * Sets the entity's sprite.
     * @param sprite Entity's new sprite.
     */
    public void setSprite(Sprite sprite) { this.sprite = sprite; }

    /**
     * Moves the entity to a new position.
     * This function must be overridden in any object that inherits this object.
     * @param tile Entity's new position.
     * @return True if success, false if fail
     */
    public abstract boolean move(Tile tile);

    /**
     * Gets the number of units of a certain type in the entity's troops.
     * @param unitType Type of the unit to count.
     * @return Amount of units of that type.
     */
    public int getUnitCount(UnitType unitType) {
        for (Units unit : getUnits())
            if (unit.getType() == unitType)
                return unit.getAmount();
        return -1;
    }
}
package role34.map.entities;

import javafx.scene.image.ImageView;

import role34.Settings;
import role34.Units;
import role34.UnitType;
import role34.CastleProductionType;
import role34.duchy.*;
import role34.map.Directions;
import role34.map.Tile;

/** Castle entity. */
public class Castle extends Entity {
    /** Castle's level. */
    private int level;
    /** Castle's riches. */
    private int riches = 0;
    /** Castle's army. */
    private Army army;
    /** Castle's owner. */
    private Duchy duke;
    /** Direction of the castle's door. */
    private Directions door;
    /** Sprite of the castle. */
    private ImageView castleView;
    /** What the castle is currently producing. */
    private CastleProductionType prodType = CastleProductionType.NONE;

    /**
     * Returns the castle's level.
     * @return Castle's level.
     */
    public int getLevel() { return level; }
    /**
     * Returns the castle's riches.
     * @return Castle's riches.
     */
    public int getRiches() { return riches; }
    /**
     * Returns the castle's army.
     * @return Castle's army.
     */
    public Army getArmy() { return army; }
    /**
     * Returns the castle's owner.
     * @return Castle's owner.
     */
    public Duchy getDuke() { return duke; }
    /**
     * Returns the direction of the castle's door.
     * @return Direction of the castle's door.
     */
    public Directions getDoor() { return door; }
    /**
     * Returns the castle's sprite
     * @return ImageView of the castle's sprite
     */
    public ImageView getCastleView() { return castleView; }
    /**
     * Returns what the castle is currently producing.
     * @return What the castle is currently producing.
     */
    public CastleProductionType getProdType() { return prodType; }

    /**
     * Returns the entity's units.
     * @return Entity's troops.
     */
    public Units[] getUnits() { return getArmy().getUnits(); }

    /**
     * Sets the castle's owner.
     * @param duke Castle's new owner.
     */
    public void setDuke(Duchy duke) { this.duke = duke; }
    /**
     * Sets the castle's sprite.
     * @param castleView New castle's sprite.
     */
    public void setCastleView(ImageView castleView) { this.castleView = castleView; }
    /**
     * Sets what the castle is currently producing.
     * @param duke Castle's new production queue.
     */
    public void setProdType(CastleProductionType prodType) { this.prodType = prodType; }

    /**
     * Creates a mobile army and sends it to a given tile.
     * @param target Destination tile.
     * @param unitCounts Amount of troops of each type.
     * @return New mobile army created (null if error).
     */
    public Army sendArmy(Tile target, int... unitCounts) {
        Army newArmy = new Army(tile);
        for (int i = 0; i < army.getUnits().length; i++) {
            Units units = army.getUnits()[i];
            newArmy.addTroop(units.getType(), unitCounts[i]);
            units.setAmount(units.getAmount() - unitCounts[i]);
        }
        if (!newArmy.goTo(target))
            return null;
        Tile t = newArmy.getPath().remove();
        newArmy.move(t);
        t.setOccupant(newArmy);
        return newArmy;
    }

    /**
     * Transfers this castle from a duke to another.
     * @param duke Castle's new owner.
     */
    public void transfer(Duchy duke) {
        this.duke.getCastles().remove(this);
        duke.addCastles(this);
        this.duke = duke;
    }

    /**
     * Returns how much this castle earns per hour.
     * @return Castle's earnings per hour.
     */
    public int getGains() {
        return level * (duke instanceof AI || duke instanceof Player ? 10 : 1);
    }

    /**
     * Moves a castle to a new tile.
     * This function is not implemented yet.
     * @param tile Castle's new position.
     * @return Always returns false for now.
     */
    public boolean move(Tile tile) { return false; }

    /** Updates the castle every turn/hour. */
    public void Update() {
        riches += getGains(); // Money gain
        // Construction queue
        // Unit creation
        if (prodType.ordinal() < UnitType.values().length) {
            if (riches >= army.getUnits()[prodType.ordinal()].getBaseCost()) {
                riches -= army.getUnits()[prodType.ordinal()].getBaseCost();
                army.addTroop(UnitType.values()[prodType.ordinal()], 1);
            }
        // Castle upgrade
        } else if (prodType.ordinal() == UnitType.values().length) {
            if (riches >= level * 500) {
                riches -= level * 500;
                level ++;
            }
        }
    }

    /**
     * Constructor for a castle.
     * @param owner Castle's owner.
     * @param tile Castle's position.
     * @param archetype 0 for Lv1 castle with no money and no troops, 1 for Player & AI, 2 for randomized for neutral castles
     */
    public Castle(Duchy owner, Tile tile, int archetype) {
        this.tile = tile;
        int typeAmount = UnitType.values().length;
        army = new Army();
        if (archetype == 2) {
            level = role34.Main.random.nextInt(5) + 1; // Between level 1 and 5
            riches = level * 250 - 100 + role34.Main.random.nextInt(201); // 250 * level + a variance of 100

            int totalPower = 10 * level - 5 + role34.Main.random.nextInt(11); // Power is level * 10 plus a variance of 5
            int dividedPower = totalPower / typeAmount;
            // This part of the code uses the power value we computed above and generates armies using this value
            for (int i = typeAmount - 1; i >= 0; i --) {
                int powerGiven = i == 0 ? totalPower : (dividedPower - dividedPower / 10 + role34.Main.random.nextInt(dividedPower / 5 + 1));
                int unitAmount = powerGiven / army.getUnits()[i].getBasePower();
                army.getUnits()[i].setAmount(unitAmount);
                totalPower -= unitAmount * army.getUnits()[i].getBasePower();
            }
        } else {
            level = 1;
            riches = 0;
            for (int i = 0; i < typeAmount; i++)
                army.getUnits()[i].setAmount(Settings.DEFAULT_GARISON_SIZE[i] * archetype);
        }

        duke = owner;
        owner.addCastles(this);
        door = tile.getNeighbourDirection(tile.getNeighbours().get(role34.Main.random.nextInt(tile.getNeighbours().size())));
    }
}
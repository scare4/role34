package role34;

import java.util.ArrayList;
import java.util.List;

import java.util.BitSet;

import javafx.event.EventHandler;

import javafx.scene.Scene;
import static javafx.scene.input.KeyCode.*;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import role34.duchy.Player;
import role34.map.Tile;
import role34.map.entities.Army;
import role34.map.entities.Castle;

/** Input handler. */
public class Input {

    /**
     * Bitset which registers if any {@link KeyCode} keeps being pressed or if it is
     * released.
     */
    private BitSet keyboardBitSet = new BitSet();

    /** Current scene. */
    private Scene scene = null;

    /** Currently hovered tile. */
    private Tile hoveredTile = null;
    /** Currently selected tile. */
    private Tile selectedTile = null;
    /** If an army is selected, its path. */
    private List<Tile> selectedPath = null;

    /** Current realm. */
    private Realm realm;

    /** Army selection window. */
    private ArmySelector armySelector = null;
    /** Clears the army selection window. */
    public void clearArmySelector() {
        armySelector = null;
    }

    /** Castle production window. */
    private CastleProdPrompt castleProdPrompt = null;
    /** Clears the castle production window. */
    public void clearCastleProdPrompt() {
        castleProdPrompt = null;
    }

    /**
     * Constructor for input handler.
     * @param scene the scene to work in.
     * @param realm the current realm.
     */
    public Input(Scene scene, Realm realm) {
        this.scene = scene;
        this.realm = realm;
        addListeners();
    }

    /**
     * Returns the currently selected tile.
     * @return Currently selected tile.
     */
    public Tile getSelectedTile() {
        return selectedTile;
    }

    /** Registers event listeners for the scene. */
    public void addListeners() {
        scene.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
        scene.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, clickEvent);
    }

    /** Removes event listeners for the scene. */
    public void removeListeners() {
        scene.removeEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
        scene.removeEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
    }

    /** Event handler for mouse entering a tile. */
    EventHandler<MouseEvent> boxHover = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            Tile tile = realm.getTileByCoords(e.getSceneX(), e.getSceneY() - Settings.STATUS_BAR_HEIGHT);
            e.consume();
            if(tile != null) {
                tile.getHbox().getStyleClass().remove("tile");
                tile.getHbox().getStyleClass().add("tileHighlight");
                // for(Tile neighbour : tile.getNeighbours()) {
                //     neighbour.getHbox().getStyleClass().remove("tile");
                //     neighbour.getHbox().getStyleClass().add("tileAccessible");
                // }
                hoveredTile = tile;
            }
            else {
                //System.out.println("invalid tile");
            }
        }
    };

    /** Event handler for mouse exiting from a tile. */
    EventHandler<MouseEvent> boxHoverEnd = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            e.consume();
            if(hoveredTile != null) {
                hoveredTile.getHbox().getStyleClass().remove("tileHighlight");
                hoveredTile.getHbox().getStyleClass().add("tile");
                // for(Tile neighbour : hoveredTile.getNeighbours()) {
                //     neighbour.getHbox().getStyleClass().remove("tileAccessible");
                //     neighbour.getHbox().getStyleClass().add("tile");
                // }
                // Main.setTileText("");
            }
        }
    };

    /** Event handler for clicking on screen. */
    EventHandler<MouseEvent> clickEvent = new EventHandler<MouseEvent>(){
        @Override
        public void handle(MouseEvent e) {
            Tile tile = realm.getTileByCoords(e.getSceneX(), e.getSceneY() - Settings.STATUS_BAR_HEIGHT);
            if (selectedTile != null && tile != null &&
                selectedTile.getOccupant() instanceof Castle &&
                ((Castle)selectedTile.getOccupant()).getDuke() instanceof Player &&
                tile.getOccupant() instanceof Castle &&
                !(((Castle)tile.getOccupant()).getDuke() instanceof Player)
            ) {
                if(armySelector == null) {
                    armySelector = new ArmySelector((Castle)selectedTile.getOccupant(), tile);
                }
            }
            if (selectedTile != null && tile != null && selectedTile == tile && ((Castle)selectedTile.getOccupant()).getDuke() instanceof Player)
                if (castleProdPrompt == null) {
                    castleProdPrompt = new CastleProdPrompt((Castle)selectedTile.getOccupant());
                }
            if(selectedTile != null) {
                selectedTile.getHbox().getStyleClass().remove("tileSelected");
                selectedTile.getHbox().getStyleClass().add("tile");
            }
            selectedTile = tile;
            if (selectedTile != null) {
                selectedTile.getHbox().getStyleClass().remove("tile");
                selectedTile.getHbox().getStyleClass().add("tileSelected");
                Main.updateInfoBar();

                if (selectedPath != null) {
                    for (Tile path : selectedPath) {
                        path.getHbox().getStyleClass().remove("tilePath");
                        path.getHbox().getStyleClass().add("tile");
                    }
                    selectedPath = null;
                }

                if (selectedTile.getOccupant() instanceof Army) {
                    selectedPath = new ArrayList<Tile>();
                    for (Tile path : ((Army)selectedTile.getOccupant()).getPath()) {
                        path.getHbox().getStyleClass().remove("tile");
                        path.getHbox().getStyleClass().add("tilePath");
                        selectedPath.add(path);
                    }
                }
            }
            else {
                Main.setTileText("");
                if (selectedPath != null)
                    for (Tile path : selectedPath) {
                        path.getHbox().getStyleClass().remove("tilePath");
                        path.getHbox().getStyleClass().add("tile");
                    }
                selectedPath = null;
            }
        }
    };

    /**
     * "Key Pressed" handler for all input events: register pressed key in the
     * bitset
     */
    private EventHandler<KeyEvent> keyPressedEventHandler = event -> {
        // register key down
        keyboardBitSet.set(event.getCode().ordinal(), true);
        event.consume();
        if(isSpace()) {
            Main.togglePause();
        }
    };

    /**
     * "Key Released" handler for all input events: unregister released key in the
     * bitset
     */
    private EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            // register key up
            keyboardBitSet.set(event.getCode().ordinal(), false);
            event.consume();
        }
    };

    /**
     * Checks if the pressed key is a specific key.
     * @param key the key to check for.
     * @return true if it is the key, false otherwise.
     */
    private boolean is(KeyCode key) {
        return keyboardBitSet.get(key.ordinal());
    }

    /**
     * Checks if the space key is pressed.
     * @return true if it is, false otherwise.
     */
    public boolean isSpace() {
        return is(SPACE);
    }
}
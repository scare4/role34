package role34;

/** Different production options for castles. */
public enum CastleProductionType {
    PIKE,
    HORSE,
    ONAGRE,
    CASTLE,
    NONE
}
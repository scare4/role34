package role34;

/** Various settings and values for the game. */
public class Settings {
    public static final double  SCENE_WIDTH = 1280;
    public static final double  SCENE_HEIGHT = 720;
    public static final double  CASTLE_WIDTH = 32;
    public static final double  CASTLE_HEIGHT = 32;
    public static final double  STATUS_BAR_HEIGHT = 50;
    public static final double  TILE_WIDTH = CASTLE_WIDTH + 2;
    public static final double  MAX_CONTACT_DISTANCE = 45;
    public static final int     DOOR_WIDTH = 3;
    public static final double  ARMY_SELECTOR_WIDTH = 210;
    public static final double  ARMY_SELECTOR_HEIGHT = 170;
    public static final double  CASTLE_PROD_PROMPT_WIDTH = 210;
    public static final double  CASTLE_PROD_PROMPT_HEIGHT = 170;
    public static final int[]   DEFAULT_GARISON_SIZE = { 10, 0, 0 };
}
package role34.duchy;

import role34.CastleProductionType;
import role34.Main;
import role34.UnitType;
import role34.map.entities.Army;
import role34.map.entities.Castle;

/** Enemy duchy with AI. */
public class AI extends Duchy {
    /** Number of hours until AI will take a decision. */
    private int delayToAction;

    /** Updates the ai (called every hour). */
    public void update() {
        if (delayToAction-- <= 0){
            delayToAction = Main.random.nextInt(15) + 16;
            for (Castle castle : getCastles()) {
                switch (Main.random.nextInt(4)) { // TODO: make production choice more targeted
                    case 0:
                        castle.setProdType(CastleProductionType.CASTLE);
                        break;
                    case 1:
                        castle.setProdType(CastleProductionType.PIKE);
                        break;
                    case 2:
                        castle.setProdType(CastleProductionType.HORSE);
                        break;
                    case 3:
                        castle.setProdType(CastleProductionType.ONAGRE);
                        break;
                }
                Castle target = castle;

                for (Castle castleCheck : Main.realm.getCastles())
                    if (!getCastles().contains(castleCheck) && castleCheck.getArmy().calculatePower() < target.getArmy().calculatePower())
                        target = castleCheck;

                if (target.getArmy().calculatePower() < castle.getArmy().calculatePower() / 2) {
                    int[] attacker = new int[UnitType.values().length];
                    for (int i = 0; i < UnitType.values().length; i++)
                        attacker[i] = castle.getArmy().getUnits()[i].getAmount() / 2;
                    Army army = castle.sendArmy(target.getTile(), attacker);
                    if (army != null)
                        Main.realm.getArmies().add(army);
                }
            }
        }
    }

    /**
     * Constructor for an AI.
     * @param name AI's duchy name.
     */
    public AI(String name) {
        super(name);
        delayToAction = Main.random.nextInt(15) + 16;
    }
}
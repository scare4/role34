package role34.duchy;

/** Player duchy. */
public class Player extends Duchy {
    /**
     * Constructor for the player.
     * @param name Player's name.
     */
    public Player(String name) {
        super(name);
        if (instance != null)
            return;
        instance = this;
    }

    /** Singleton of this class: ensures that only one Player is created. */
    public static Player instance = null;
}
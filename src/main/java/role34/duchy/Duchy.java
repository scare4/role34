package role34.duchy;

import java.util.ArrayList;
import java.util.List;

import role34.map.entities.Castle;

/** Neutral duchy. */
public class Duchy {
    /** Duchy's name. */
    private String name;
    /** Duchy's identifier. */
    private int ID;
    /** Duchy's castles. */
    private List<Castle> castles;

    /**
     * Returns the duchy's name.
     * @return Duchy's name.
     */
    public String getName() { return name; }
    /**
     * Returns the duchy's identifier.
     * @return Duchy's identifier.
     */
    public int getID() { return ID; }
    /**
     * Returns the duchy's castles.
     * @return Duchy's castle list.
     */
    public List<Castle> getCastles() { return castles; }

    /**
     * Checks whether this duke owns the given castle.
     * @param castle Given castle.
     * @return True if this duke owns the given castle, false otherwise.
     */
    public boolean isOwner(Castle castle) {
        return castles.contains(castle);
    }

    /**
     * Computes the duke's total wealth.
     * @return Duke's total wealth.
     */
    public int getRiches() {
        int riches = 0;
        for(Castle castle : getCastles())
            riches += castle.getRiches();
        return riches;
    }

    /**
     * Adds several castles to this duke.
     * @param castles Castles to add.
     * @return Number of castles successfully added.
     */
    public int addCastles(Castle... castles) {
        int castleCount = 0;
        for (Castle castle : castles) {
            if(!isOwner(castle)) {
                this.castles.add(castle);
                castleCount++;
            }
        }
        return castleCount;
    }

    /**
     * Constructor for a duchy.
     * @param name Duchy's name.
     */
    public Duchy(String name) {
        this.name = name;
        castles = new ArrayList<Castle>();
    }
}
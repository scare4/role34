package role34;

/** Unit groups. */
public class Units {
    /** Type of the units. */
    private UnitType type;
    /** Base health of the units. */
    private int baseHealth;
    /** Base power of the units. */
    private int basePower;
    /** Base cost of the units. */
    private int baseCost;
    /** Movement speed of the units. */
    private int speed;
    /** Health of the first unit in the group. */
    private int health;
    /** Amount of units in the group. */
    private int amount = 0;

    /**
     * Returns the type of the units.
     * @return Type of the units.
     */
    public UnitType getType()  { return type;       }
    /**
     * Returns the base health of the units.
     * @return Base health of the units.
     */
    public int getBaseHealth() { return baseHealth; }
    /**
     * Returns the base power of the units.
     * @return Base power of the units.
     */
    public int getBasePower()  { return basePower;  }
    /**
     * Returns the base cost of the units.
     * @return Base cost of the units.
     */
    public int getBaseCost()   { return baseCost;   }
    /**
     * Returns the movement speed of the units.
     * @return Movement speed of the units.
     */
    public int getSpeed()      { return speed;      }
    /**
     * Returns the health of the first unit in the group.
     * @return Health of the first unit in the group.
     */
    public int getHealth()     { return health;     }
    /**
     * Returns the power of the units.
     * @return Power of the units.
     */
    public int getPower()      { return basePower;  }
    /**
     * Returns the amount of units in the group.
     * @return Amount of units in the group.
     */
    public int getAmount()     { return amount;     }
    /**
     * Sets the amount of units in the group.
     * @param amount the new amount.
     */
    public void setAmount(int amount) { this.amount = amount; }

    /**
     * Deals damage (substracts health) to the unit, if health falls to 0, removes a unit from the count.
     * @param damage Damage to deal.
     * @return Unit's new health.
     */
    public int damage(int damage) {
        health -= damage;
        if (health <= 0) {
            amount--;
            health = baseHealth;
        }
        return health;
    }

    /**
     * Constructor for a unit group.
     * @param type Type of units in the group.
     * @param amount Amount of units in the group.
     */
    public Units(UnitType type, int amount) {
        this.type = type;
        this.amount = amount;
        switch (type) {
            case PIKE:
                speed = 2;
                baseHealth = 1;
                basePower = 1;
                baseCost = 100;
                break;
            case HORSE:
                speed = 6;
                baseHealth = 3;
                basePower = 5;
                baseCost = 500;
                break;
            case ONAGRE:
                speed = 1;
                baseHealth = 5;
                basePower = 10;
                baseCost = 1000;
                break;
        }
        health = baseHealth;
    }

    /**
     * Creates an array for storing info about several unit types.
     * @return Created empty array.
     */
    public static Units[] createEmptyUnits() {
        UnitType[] types = UnitType.values();
        Units[] units = new Units[types.length];
        for (int i = 0; i < types.length; i++)
            units[i] = new Units(types[i], 0);
        return units;
    }
}
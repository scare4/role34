package role34;

import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import role34.map.entities.Castle;
import role34.CastleProductionType;

/** Castle production window. */
public class CastleProdPrompt {
    /** Main box of this window. */
    private VBox selectorBox;
    /** Table of all possible choice buttons. */
    private Button[] buttons;

    /** Window's title. */
    private Text titleText = new Text();
    /** Castle to change the production type of. */
    private Castle castle;

    /**
     * Creates a prompt window that asks which production type this castle does.
     * @param castle Castle to change the production type of.
     */
    public CastleProdPrompt(Castle castle) {
        Main.pause();
        this.castle = castle;

        selectorBox = new VBox();
        selectorBox.getStyleClass().add("window");
        selectorBox.setPrefSize(Settings.CASTLE_PROD_PROMPT_WIDTH, Settings.CASTLE_PROD_PROMPT_HEIGHT);

        titleText.setText("Current: " + castle.getProdType().toString() + ".\n");
        selectorBox.getChildren().add(titleText);

        int buttonNumber = CastleProductionType.values().length + 1;
        buttons = new Button[buttonNumber];
        for (int i = 0; i < buttonNumber; i++) {
            if (i == buttonNumber - 1) {
                buttons[i] = new Button("Cancel");
                buttons[i].setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        Main.root.getChildren().remove(selectorBox);
                        Main.unPause();
                        Main.input.clearCastleProdPrompt();
                    }
                });
            } else {
                buttons[i] = new Button(CastleProductionType.values()[i].toString());

                buttons[i].setOnAction(new EventHandlerData<ActionEvent>(i) {
                    @Override
                    public void handle(ActionEvent e) {
                        castle.setProdType(CastleProductionType.values()[getData()]);
                        Main.root.getChildren().remove(selectorBox);
                        Main.unPause();
                        Main.input.clearCastleProdPrompt();
                    }
                });
            }
            selectorBox.getChildren().add(buttons[i]);
        }
        selectorBox.setLayoutX(Settings.SCENE_WIDTH / 2 - Settings.ARMY_SELECTOR_WIDTH / 2);
        selectorBox.setLayoutY(Settings.SCENE_HEIGHT / 2 - Settings.ARMY_SELECTOR_HEIGHT / 2);
        selectorBox.setAlignment(Pos.TOP_LEFT);
        Main.root.getChildren().add(selectorBox);
    }
}
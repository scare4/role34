package role34;

import javafx.event.EventHandler;

import javafx.event.Event;

/**
 * Custom event handler that can store an integer as data.
 * @param <T> the type of the event handler.
 */
public abstract class EventHandlerData<T extends Event> implements EventHandler<T> {
    /** Extra data to store. */
    private int data;

    /**
     * Returns the extra data to store.
     * @return Extra data to store.
     */
    public int getData() { return data; }

    @Override
    public abstract void handle(T e);

    /**
     * Creates an event handler with predefined data.
     * @param data the data to predefine.
     */
    public EventHandlerData(int data) {
        super();
        this.data = data;
    }
}
package role34;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/** Sprite objects. */
public abstract class Sprite {
    /** ImageView object displayed on screen. */
    private ImageView imageView;
    /** Layer to display upon. */
    private Pane layer;
    /** Horizontal coordinate of the sprite. */
    protected double x;
    /** Vertical coordinate of the sprite. */
    protected double y;
    /** Width of the sprite. */
    private double w;
    /** Height of the sprite. */
    private double h;

    /**
     * Returns the horiozontal coordinate of the sprite.
     * @return Horizontal coordinate of the sprite.
     */
    public double getX() { return x; }
    /**
     * Returns the vertical coordinate of the sprite.
     * @return Vertical coordinate of the sprite.
     */
    public double getY() { return y; }
    /**
     * Returns the width of the sprite.
     * @return Width of the sprite.
     */
    public double getWidth()  { return w; }
    /**
     * Returns the height of the sprite.
     * @return Height of the sprite.
     */
    public double getHeight() { return h; }

    /**
     * Moves the sprite on the x axis.
     * @param x New x axis coordinate.
     */
    public void setX(double x) { this.x = x; }
    /**
     * Moves the sprite on the y axis.
     * @param x New y axis coordinate.
     */
    public void setY(double y) { this.y = y; }

    /**
     * Returns the horizontal coordinate of the center of the sprite.
     * @return Horizontal coordinate of the center of the sprite.
     */
    public double getCenterX() { return x + w * 0.5; }
    /**
     * Returns the vertical coordinate of the center of the sprite.
     * @return Vertical coordinate of the center of the sprite.
     */
    public double getCenterY() { return y + h * 0.5; }

    /** Adds this sprite to a layer. */
    public void addToLayer()      { this.layer.getChildren().add(this.imageView);    }
    /** Removes this sprite from a layer. */
    public void removeFromLayer() { this.layer.getChildren().remove(this.imageView); }

    /**
     * Returns the ImageView of the sprite.
     * @return ImageView of the sprite.
     */
    protected ImageView getView() { return imageView; }

    /** Moves the sprite to its coordinates. */
    public void updateUI() { imageView.relocate(x, y); }

    /**
     * Sprite constructor.
     * @param layer Layer to display the sprite upon.
     * @param image Image of the sprite.
     * @param x Horizontal axis coordinate of the sprite.
     * @param y Vertical axis coordinate of the sprite.
     */
    public Sprite(Pane layer, Image image, double x, double y) {
        this.layer = layer;
        this.x = x;
        this.y = y;

        this.imageView = new ImageView(image);
        this.imageView.relocate(x, y);

        this.w = image.getWidth();
        this.h = image.getHeight();

        addToLayer();
    }
}
package role34;

import java.util.ArrayList;
import java.util.List;

import role34.map.Tile;
import role34.map.entities.*;
import role34.map.tilemaps.Tilemap;
import role34.duchy.*;

/** A realm. */
public class Realm {
    /** Realm's tilemap. */
    private Tilemap tilemap;
    /** Armies roaming in the realm. */
    private List<Army> armies;

    /**
     * Returns the list of castles in the realm.
     * @return List of castles in the realm.
     */
    public List<Castle> getCastles() { return tilemap.getCastles(); }
    /**
     * Returns the list of armies in the realm.
     * @return List of armies in the realm.
     */
    public List<Army> getArmies()    { return armies;               }
    /**
     * Returns the list of tiles in the realm.
     * @return List of tiles in the realm.
     */
    public List<Tile> getTiles()     { return tilemap.getTiles();   }
    /**
     * Returns the list of duchies in the realm.
     * @return List of duchies in the realm.
     */
    public List<Duchy> getDuchies()  { return tilemap.getDuchies(); }

    /** Time in the realm. */
    private int time = 0;
    /**
     * Returns the current time in the realm.
     * @return Current time in the realm.
     */
    public int getTime() { return time; }

    /**
     * Finds a tile from on screen coordinates.
     * @param x X axis position of the point to look for.
     * @param y Y axis position of the point to look for.
     * @return Tile found or null.
     */
    public Tile getTileByCoords(double x, double y) {
        for (Tile tile : tilemap.getTiles()) {
            if (tile.proximity(x, y)) {
                return tile;
            }
        }
        return null;
    }

    /**
     * Steps time by 1.
     * @return New time.
     */
    public int stepTime() {
        return ++time;
    }

    /**
     * Adds tiles to the realm.
     * @param tiles An undefined number of tile objects to add.
     * @return Number of tiles added.
     */
    public int addTiles(Tile... tiles) {
        int tileCount = 0;
        for (Tile tile : tiles) {
            this.tilemap.addTile(tile);
            tileCount++;
        }
        return tileCount;
    }

    /**
     * Realm constructor.
     * @param tilemap Tilemap to generate the realm from.
     */
    public Realm(Tilemap tilemap) {
        armies = new ArrayList<Army>();
        this.tilemap = tilemap;
        time = 0;
    }
}
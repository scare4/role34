package role34;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.ImageView;
import role34.duchy.AI;
import role34.duchy.Duchy;
import role34.map.Tile;
import role34.map.entities.*;
import role34.map.tilemaps.*;

/** Main class with entry point. */
public class Main extends Application {

    public static Random random = new Random();

    private Pane playfieldLayer;
    private Scene scene;
    private AnimationTimer gameLoop;

    /** Text element for displaying time in the info bar. */
    private static Text timeText = new Text();
    /** Text element for displaying tile info in the info bar. */
    private static Text tileText = new Text();

    /**
     * Updates the tile info text in the info bar.
     * @param text New text to display in the info bar.
     */
    public static void setTileText(String text) {
        tileText.setText(text);
    }

    /** Current playing realm to conquer. */
    public static Realm realm;

    /** Sprite for background map image. */
    private MapSprite mapSprite;

    /** Image for ennemy castle. */
    private Image castleEImage;
    /** Image for player castle. */
    private Image castlePImage;
    /** Image for neutral castle. */
    private Image castleNImage;

    /** Image for army. */
    public static Image armyImage;

    /** Input handler. */
    public static Input input;

    /** Display root. */
    public static Group root;

    /** List of armies to delete once armies have been processed. */
    private List<Army> deadArmies;

    /** State of the pause. */
    private static boolean pause;

    /**
     * Checks if the game is paused.
     * @return true if paused, false if not.
     */
    public static boolean isPaused() {
        return pause;
    }

    /**
     * Pauses the game (no effect if the game is already paused)
     */
    public static void pause() {
        pause = true;
    }

    /**
     * Unpauses the game (no effect if the game is already unpaused)
     */
    public static void unPause() {
        pause = false;
    }

    /**
     * Toggles the pause.
     * @return the new state of the pause.
     */
    public static boolean togglePause() {
        pause = !pause;
        return pause;
    }

    @Override
    public void start(Stage primaryStage) {
        root = new Group();
        scene = new Scene(root, Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT + Settings.STATUS_BAR_HEIGHT);
        scene.getStylesheets().add(getClass().getResource("/css/game.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Role 34");
        primaryStage.show();

        // create layers
        playfieldLayer = new Pane();
        root.getChildren().add(playfieldLayer);

        loadGame();
        createInfoBar();

        createTilemapDisplay();

        gameLoop = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
            }
        };
        gameLoop.start();
    }

    /** Loads images and initialises the realm and the input handler. */
    private void loadGame() {
        //input.addListeners();
        Image mapImage = new Image(getClass().getResource("/images/map.png").toExternalForm(), Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT, true, true);
        mapSprite = new MapSprite(playfieldLayer, mapImage);
        castleEImage = new Image(getClass().getResource("/images/castleEnemy.png").toExternalForm(), Settings.CASTLE_WIDTH, Settings.CASTLE_HEIGHT, true, true);
        castlePImage = new Image(getClass().getResource("/images/castlePlayer.png").toExternalForm(), Settings.CASTLE_WIDTH, Settings.CASTLE_HEIGHT, true, true);
        castleNImage = new Image(getClass().getResource("/images/castleNeutral.png").toExternalForm(), Settings.CASTLE_WIDTH, Settings.CASTLE_HEIGHT, true, true);
        armyImage = new Image(getClass().getResource("/images/army.png").toExternalForm(), Settings.CASTLE_WIDTH, Settings.CASTLE_HEIGHT, true, true);
        realm = new Realm(new Map1());
        input = new Input(scene, realm);
        deadArmies = new ArrayList<Army>();
    }

    /** Creates the info bar at the top of the screen. */
    private void createInfoBar() {
        HBox infoBar = new HBox();
        updateInfoBar();
        infoBar.getChildren().addAll(timeText, tileText);
        infoBar.getStyleClass().add("infoBar");
        infoBar.setPrefSize(Settings.SCENE_WIDTH, Settings.STATUS_BAR_HEIGHT);
        root.getChildren().add(infoBar);
    }

    /** Creates the on-screen tiles. */
    private void createTilemapDisplay() {
        for(Tile tile : realm.getTiles()) {
            HBox tilebox = new HBox();
            tilebox.getStyleClass().add("tile");
            tilebox.setPrefSize(Settings.TILE_WIDTH, Settings.TILE_WIDTH);
            tilebox.setLayoutX(tile.getX());
            tilebox.setLayoutY(tile.getGraphicalY());
            tilebox.addEventFilter(MouseEvent.MOUSE_ENTERED, input.boxHover);
            tilebox.addEventFilter(MouseEvent.MOUSE_EXITED, input.boxHoverEnd);

            if(tile.getOccupant() instanceof Castle) {
                Castle c = (Castle)tile.getOccupant();
                ImageView castleView = null;
                if (c.getDuke() instanceof role34.duchy.Player)  castleView = new ImageView(castlePImage);
                else if (c.getDuke() instanceof role34.duchy.AI) castleView = new ImageView(castleEImage);
                else                                             castleView = new ImageView(castleNImage);
                castleView.relocate(tile.getX() + 2, tile.getGraphicalY() + 2);
                tilebox.getChildren().add(castleView);
                c.setCastleView(castleView);

                HBox doorbox = new HBox();
                doorbox.setPrefSize(Settings.TILE_WIDTH + 2, Settings.TILE_WIDTH + 2);
                doorbox.setLayoutX(tile.getX() - 1);
                doorbox.setLayoutY(tile.getGraphicalY() - 1);

                switch(c.getDoor()) {
                    case NORTH:
                        doorbox.getStyleClass().add("castleNorth");
                        break;
                    case EAST:
                        doorbox.getStyleClass().add("castleEast");
                        break;
                    case SOUTH:
                        doorbox.getStyleClass().add("castleSouth");
                        break;
                    case WEST:
                        doorbox.getStyleClass().add("castleWest");
                        break;
                }
                root.getChildren().add(doorbox);
            }

            tile.setHBox(tilebox);
            root.getChildren().add(tilebox);
        }
    }

    /** Updates the info bar. */
    public static void updateInfoBar() {
        timeText.setText("Time : Day #" + days + ", " + hours + "h");

        Tile tile = input.getSelectedTile();
        String tileText = "        ";
        if (tile != null && tile.getOccupant() != null) {
            Entity occupant = tile.getOccupant();
            if (occupant instanceof Castle) {
                tileText += "Level : " + ((Castle)occupant).getLevel() + "    ";
                tileText += "Money : " + ((Castle)occupant).getRiches() + " (" + ((Castle)occupant).getGains() + " / h)    ";
                tileText += "Owner : " + ((Castle)occupant).getDuke().getName() + "    ";
            }
            tileText += "   Garison: ";
            tileText += occupant.getUnitCount(UnitType.PIKE)       + " Pikemen,   ";
            tileText += occupant.getUnitCount(UnitType.HORSE)      + " Horsemen,   ";
            tileText += occupant.getUnitCount(UnitType.ONAGRE)     + " Onagres";
        } else
            tileText = "";
        Main.setTileText(tileText);
    }

    /** Called once every in game hour (every real second) */
    private void tick() {
        if (++hours >= 24) {
            days ++;
            hours = 0;
        }

        updateInfoBar();

        // Code to be executed every in game hour
        for (Castle castle : realm.getCastles())
            castle.Update();

        for(Army army : realm.getArmies()) {
            if (army.getFormation() > 0) { // Forming armies
                army.stepForm();
                continue;
            }
            if (army.getPath() == null || army.getPath().isEmpty()) // Stuck armies
                continue;

            if (army.getTile().moveOccupant(army.getPath().getFirst())) { // Moving armies
                army.move(army.getPath().getFirst());
                army.getPath().remove();
            }
            else {
                if(army.getPath().getFirst() == army.getDestination()) { // Reached destination
                    if(((Castle)army.getDestination().getOccupant()).getDuke() == army.getDuke()) { // Merging into an army
                        army.mergeWith(((Castle)army.getDestination().getOccupant()).getArmy());
                        deadArmies.add(army);
                    }
                    else { // Attacking a castle
                        for(Units units : army.getUnits()) {
                            for(int i = 0; i < units.getAmount(); i++) {
                                for(int j = 0; j < units.getBasePower(); j++) {
                                    if(units.getAmount() <= 0) {
                                        break;
                                    }
                                    do {
                                        Units u = ((Castle)army.getDestination().getOccupant()).getArmy().getUnits()[role34.Main.random.nextInt(UnitType.values().length)];
                                        if(u.getAmount() > 0) {
                                            u.damage(1);
                                            units.damage(1);
                                            break;
                                        }
                                        int uh = 0;
                                        for(Units ucheck : ((Castle)army.getDestination().getOccupant()).getArmy().getUnits()) {
                                            uh += ucheck.getAmount();
                                        }
                                        if(uh <= 0) { // Castle fell
                                            ((Castle)army.getDestination().getOccupant()).transfer(army.getDuke());
                                            army.getDestination().getHbox().getChildren().remove(((Castle)army.getDestination().getOccupant()).getCastleView());
                                            if (((Castle)army.getDestination().getOccupant()).getDuke() instanceof role34.duchy.Player)  ((Castle)army.getDestination().getOccupant()).setCastleView(new ImageView(castlePImage));
                                            else if (((Castle)army.getDestination().getOccupant()).getDuke() instanceof role34.duchy.AI) ((Castle)army.getDestination().getOccupant()).setCastleView(new ImageView(castleEImage));
                                            else                                                                                         ((Castle)army.getDestination().getOccupant()).setCastleView(new ImageView(castleNImage));
                                            army.getDestination().getHbox().getChildren().add(((Castle)army.getDestination().getOccupant()).getCastleView());
                                            break;
                                        }
                                    } while(true);
                                }
                            }
                        }
                        int uh = 0;
                        for(Units ucheck : army.getUnits()) {
                            uh += ucheck.getAmount();
                        }
                        if(uh <= 0) { // Army fell
                            deadArmies.add(army);
                            army.mergeWith(((Castle)army.getDestination().getOccupant()).getArmy());
                        }

                    }
                }
                army.goTo(army.getDestination()); // Path recalculation
            }
        }
        for(Army army : deadArmies) {
            realm.getArmies().remove(army);
        }
        for(Duchy duchy : realm.getDuchies()) {
            if(duchy instanceof AI)
                ((AI)duchy).update();
        }
        deadArmies.clear();

    }

    /** Last recorded time in seconds, used to determine update rates. */
    private long lastTime;
    /** Day count. */
    private static long days = 1;
    /** Hour count. */
    private static long hours = 0;

    /** Called every update. */
    private void update() {
        if (lastTime != System.currentTimeMillis() / 1000 && !isPaused())
            tick();
        lastTime = System.currentTimeMillis() / 1000;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
package role34;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

/** Custom sprite for map background. */
class MapSprite extends Sprite {
    /**
     * Creates a sprite for the background map.
     * @param layer Layer to display the sprite upon.
     * @param image Image for the sprite.
     */
    public MapSprite(Pane layer, Image image) {
        super(layer, image, 0, Settings.STATUS_BAR_HEIGHT);
    }
}